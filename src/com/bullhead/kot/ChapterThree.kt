package com.bullhead.kot

import kotlin.random.Random

fun main() {
    val strings = listOf("hello", "osama", "bin", "omar")
    println(strings.last())
    println(strings.joinToString(";"))

    //SECTION 1
    println("Hello" toOsama "Infix")
    println("The random is" betweenRandom "between us" toOsama " to Osama")

    //SECTION 2
    val button = Button()
    button.showOff()
    button.setFocus(true)
    button.click()
}

/*
SECTION 1
BOOK REF 3.4.3
INFIX function syntax
 */
infix fun String.toOsama(other: String) = "$this {Osama} $other"

infix fun String.betweenRandom(other: String) = "$this ${Random(1000).nextInt()} $other"

/*
SECTION 2
BOOK REF 4.1.1
INTERFACE WITH DEFAULT METHOD
Default method with body
Need to explicitly implement if definition conflict in more than one implemented interfaces
*/
interface Clickable {
    fun click()
    fun showOff() = println("I'm clickable")
}

interface Focusable {
    fun setFocus(focus: Boolean) = println("I ${if (focus) "got" else "lost"} focus")
    fun showOff() = println("I'm focusable")
}

class Button : Clickable, Focusable {
    override fun showOff() {
        super<Clickable>.showOff()
        super<Focusable>.showOff()
    }

    override fun click() {
        println("Why you clicked me?")
    }

}
/*
SECTION 3
BOOK REF 4.2.3
IMPLEMENTING PROPERTIES DECLARED IN INTERFACES
 */
interface User {
    val nickname: String
}

class PrivateUser(override val nickname: String) : User
class SubscribingUser(private val email: String) : User {
    override val nickname: String
        get() = email.substringBefore('@')
}

class IsiUser : User {
    override val nickname = "user@isi"
}